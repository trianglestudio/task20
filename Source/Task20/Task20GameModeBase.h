// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Task20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TASK20_API ATask20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
